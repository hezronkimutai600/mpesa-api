# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)


User1 = User.create(first_name: "Hezzie", last_name: "Kim", email:"hezkim@gmail.com")
User2 = User.create(first_name: "den", last_name: "Gikia", email:"dengikia@email.com")
User3 = User.create(first_name: "Skittles", last_name: "Jamba", email:"skJa@gmail.com")
