class UsersController < ApplicationController
    def index
        @Users = User.all
        render json: @Users
    end

    def show
        @User = User.find(params[:id])
        render json: @User
    end

    def create
        @User = User.create(
            first_name:params[:first_name],
            last_name:params[:last_name],
            email: params[:email]
        )
        render json: @User
    end

    def update
        @User = User.find(params[:id])
        @User.update(
            first_name:params[:first_name],
            last_name:params[:last_name],
            email: params[:email]
        )
        render json: @User
    end

    def destroy
        @Users = User.all
        @User = User.find(params[:id])
        @User.destroy
        render json: @Users
    end

end
